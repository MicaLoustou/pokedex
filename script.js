const searchInput = document.getElementById('searchInput');
const searchIcon = document.getElementById('searchIcon');
const captureButton = document.getElementById('captureButton');
const pokeDataContainer = document.querySelector('.main .pokeDataContainer .pokeData');
const pokeImgContainer = document.querySelector('.main .pokeDataContainer .pokeImg img');

let captured = [];
const pokemon = {};
let { pokeId, pokeName, pokeImg, pokeLocation, pokeAbility } = pokemon;

const showCaptured = (pokemons) => {
    const fragment = document.createDocumentFragment()
    let i = 0;

    for (pokeCaptured of pokemons) {

        const capturedDiv = document.createElement('div');
        capturedDiv.classList.add('captured')

        const capturedImg  = document.createElement('img');
        capturedImg.src = pokeCaptured;

        capturedDiv.appendChild(capturedImg);
        fragment.appendChild(capturedDiv);

        const close = document.createElement('i')
        close.classList.add('fas', 'fa-times')
        capturedDiv.append(close)
        close.setAttribute('data-poke-id', i++)

        capturedDiv.addEventListener('mouseover', () => {
            capturedImg.style.opacity = '0.4'
            close.style.opacity = '1'
        })
    
        capturedDiv.addEventListener('mouseleave', () => {
            capturedImg.style.opacity = '1'
            close.style.opacity = '0'
        })

        close.addEventListener('click', (e) => {
            if (confirm('Are you sure you want to delete your pokemon?')) {
                const pokeID = e.target.dataset.pokeId
                let deleteCaptured = JSON.parse(localStorage.getItem('captured'))
                deleteCaptured.splice(pokeID, 1)
                localStorage.setItem('captured', JSON.stringify(deleteCaptured))
                showCaptured(deleteCaptured)
            }
        })
    }
    document.querySelector('.main .capturedContainer').textContent = '';
    document.querySelector('.main .capturedContainer').append(fragment);
}

if (localStorage.getItem('captured') || localStorage.length > 0) {
    showCaptured(JSON.parse(localStorage.getItem('captured')))
}

searchIcon.addEventListener('click', () => {
    axios(`https://pokeapi.co/api/v2/pokemon/${searchInput.value.toLowerCase()}`)
        .then(async ({ data: pokeAPI }) => {

            const { data: location_url } = await axios(pokeAPI.location_area_encounters);
            
            pokeImgContainer.src = pokeAPI.sprites.front_default;
            pokeDataContainer.innerHTML = `<p>${pokeAPI.name.toUpperCase()}</p>`
            location_url.length > 0 ? pokeDataContainer.innerHTML += `<p>Find it in: ${location_url[0].location_area.name}</p>` : null
            pokeAPI.abilities.length > 0 ? pokeDataContainer.innerHTML += `<p>Ability: ${pokeAPI.abilities[0].ability.name}</p>` : null;

            pokeId = pokeAPI.id;
            pokeName = pokeAPI.name;
            pokeImg = pokeAPI.sprites.front_default;
            location_url.length > 0 ? pokeLocation = location_url[0].location_area.name : null;
            pokeAPI.abilities.length > 0 ? pokeAbility = pokeAPI.abilities[0].ability.name : null;
        })
        .catch(() => {
            pokeImgContainer.src = '404.png'
            pokeDataContainer.textContent = 'Pokemon not found';
        })
    searchInput.value = '';
})

captureButton.addEventListener('click', () => {
    if (pokeId) {
        const capturedPokemon = localStorage.getItem('captured')
        if (!capturedPokemon || capturedPokemon.length == 0) {
            captured = []
            captured.push(pokeImg)
            localStorage.setItem('captured', JSON.stringify(captured))
            showCaptured(captured)
        } else {
            captured = []
            captured = JSON.parse(localStorage.getItem('captured'))
            if (captured.length < 6) {
                captured.push(pokeImg)
                localStorage.setItem('captured', JSON.stringify(captured))
                showCaptured(captured)
            } else { alert('maximum of pokemons captured') }
        }

    } else { alert('Pokemon not selected') }
})