# Pokedex 🐞
---
I made a dynamic and responsive Pokedex with html5, css3 and JavaScript.

## Watch the project on Netlify 👀
---
### [Netlify: Pokedex](https://kind-brahmagupta-2845c7.netlify.app).

## What does the project look like? 🎨
---

1. Index.
![index](https://bytebucket.org/MicaLoustou/pokedex/raw/b744dbf7f12674fab9493894d3d517dcbf945872/index.jpeg)

## Tools 🔧
---
For build this proyect i used:

* HTML 5.
* CSS 3.
* JavaScript ES6.
* Axios.
* APIs REST like - PokeApi.

## To run this proyect 🏃‍♂️
---
1. Clone this repository.
2. Double click on the index.html file.

---
⌨️ with ❤️ by [Mica Loustou](https://bitbucket.org/MicaLoustou/) 😊